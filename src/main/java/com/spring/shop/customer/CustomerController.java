package com.spring.shop.customer;

import com.spring.shop.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/shop/customer")
public class CustomerController {

    private final CustomerService service;

    @Autowired
    public CustomerController(CustomerService service) {
        this.service = service;
    }

    @GetMapping("/show")
    public List<Customer> getAllCustomers() {
        ArrayList<Customer> list = new ArrayList<>();
        return list;
    }

    @GetMapping("/show/{id}")
    public Customer getCustomer(@PathVariable Long id) {
        return new Customer();
    }

    @PostMapping(path = "/save",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void saveCustomer(@RequestBody Customer customer) { // TODO change output method

    }

    @PutMapping("/update-create/{id}")
    public void updateOrCreateCustomer(@PathVariable Long id,
                                       @RequestBody Customer customer) { // TODO change output method

    }

    @PatchMapping("/update/{id}")
    public void updateCustomer(@PathVariable Long id,
                               @RequestBody Customer customer) { // TODO change output method

    }

    @DeleteMapping("/delete/{id}")
    public void deleteCustomer(@PathVariable Long id) {

    }

}
