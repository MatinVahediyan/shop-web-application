package com.spring.shop.model;

import com.spring.shop.model.enumeration.CustomerType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.util.List;

@Entity(name = "Individual")
public class Individual extends Customer {
    @Column(
            name = "first_name",
            nullable = false
    )
    private String firstName;
    @Column(
            name = "last_name",
            nullable = false
    )
    private String lastName;
    @Column(
            name = "middle_name"
    )
    private String middleName;
    @Email
    @Column(
            name = "email"
    )
    private String email;
    @Pattern(regexp = "(^$|[0-9]{10})")
    @Column(
            name = "phone_number"
    )
    private String phone;
    @Column(
            name = "locale",
            columnDefinition = "VARCHAR(255) DEFAULT 'English'"
    )
    private String locale;

    public Individual() {
    }

    public Individual(CustomerType type,
                      List<Details> details,
                      String description,
                      BatchCode batchCode,
                      Entitlement entitlement,
                      String firstName,
                      String lastName,
                      String middleName,
                      String email,
                      String phone,
                      String locale) {
        super(type, details, description, batchCode, entitlement);
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.email = email;
        this.phone = phone;
        this.locale = locale;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    @Override
    public String toString() {
        return "Individual{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", locale='" + locale + '\'' +
                '}';
    }
}
