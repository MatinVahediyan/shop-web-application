package com.spring.shop.model.enumeration;

public enum CustomerType {
    Individual,
    Company
}
