package com.spring.shop.model.enumeration;

public enum Rehost {
    Enable,
    Disable,
    Leave_as_it_is,
    Specify_at_entitlement_time
}
