package com.spring.shop.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity(name = "feature")
@Table(
        name = "features"
)
public class Feature {
    @Id
    @SequenceGenerator(
            name = "features_sequence",
            sequenceName = "features_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "features_sequence"
    )
    private Long id;
    @Column(
            name = "name"
    )
    @Size(max = 50)
    private String name;
    @Column(
            name = "description",
            columnDefinition = "VARCHAR(1000)"
    )
    private String description;

    @ManyToOne
    @JoinColumn(
            name = "product_id",
            referencedColumnName = "id",
            nullable = false,
            foreignKey = @ForeignKey(
                    name = "product_feature_fk"
            )
    )
    private Product product;

    @ManyToOne
    @JoinColumn(
            name = "batch_code_id",
            referencedColumnName = "id",
            nullable = false,
            foreignKey = @ForeignKey(
                    name = "batch_code_feature_fk"
            )
    )
    private BatchCode batchCode;

    public Feature() {
    }

    public Feature(String name,
                   String description,
                   Product product,
                   BatchCode batchCode) {
        this.name = name;
        this.description = description;
        this.product = product;
        this.batchCode = batchCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BatchCode getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(BatchCode batchCode) {
        this.batchCode = batchCode;
    }

    @Override
    public String toString() {
        return "Feature{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", product=" + product +
                ", batchCode=" + batchCode +
                '}';
    }
}
