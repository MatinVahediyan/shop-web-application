package com.spring.shop.model;


import com.spring.shop.model.enumeration.CustomerType;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Company")
public class Company extends Customer {

    @Column(
            name = "name",
            nullable = false
    )
    private String name;

    @Column(
            name = "phone"
    )
    @Pattern(regexp = "(^$|[0-9]{8})")
    private String phone;
    @Column(
            name = "fax"
    )
    private String fax;

    @OneToMany(
            mappedBy = "company",
            orphanRemoval = true,
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private List<Contact> contacts = new ArrayList<>();

    public Company() {

    }

    public Company(CustomerType type,
                   List<Details> details,
                   String description,
                   BatchCode batchCode,
                   Entitlement entitlement,
                   String name,
                   String phone,
                   String fax,
                   List<Contact> contacts) {
        super(type, details, description, batchCode, entitlement);
        this.name = name;
        this.phone = phone;
        this.fax = fax;
        this.contacts = contacts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public String toString() {
        return "Company{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", fax='" + fax + '\'' +
                ", contacts=" + contacts +
                '}';
    }
}
