package com.spring.shop.model;

import com.spring.shop.model.enumeration.LockingType;
import com.spring.shop.model.enumeration.Rehost;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Product")
@Table(
        name = "products"
)
public class Product {
    @Id
    @SequenceGenerator(
            name = "products_sequence",
            sequenceName = "products_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "products_sequence"
    )
    private Long id;
    @Column(
            name = "name"
    )
    @Size(max = 50)
    private String name;

    @Enumerated(EnumType.STRING)
    private LockingType lockingType;

    @Enumerated(EnumType.STRING)
    private Rehost rehost;

    @Column(
            name = "description",
            columnDefinition = "VARCHAR(1000)"
    )
    private String description;

    @OneToMany(
            mappedBy = "product",
            orphanRemoval = true,
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private List<Feature> features = new ArrayList<>();
    @ManyToOne
    @JoinColumn(
            name = "batch_code_id",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(
                    name = "batch_code_product_fk"
            )
    )
    private BatchCode batchCode;

    public Product() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LockingType getLockingType() {
        return lockingType;
    }

    public void setLockingType(LockingType lockingType) {
        this.lockingType = lockingType;
    }

    public Rehost getRehost() {
        return rehost;
    }

    public void setRehost(Rehost rehost) {
        this.rehost = rehost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public BatchCode getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(BatchCode batchCode) {
        this.batchCode = batchCode;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lockingType=" + lockingType +
                ", rehost=" + rehost +
                ", description='" + description + '\'' +
                ", features=" + features +
                ", batchCode=" + batchCode +
                '}';
    }
}
