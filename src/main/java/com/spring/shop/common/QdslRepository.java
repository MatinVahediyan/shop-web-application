package com.spring.shop.common;

import com.querydsl.core.types.QBean;

import java.util.List;

public interface QdslRepository<T, ID> {
//    <S extends T> S save(S var1);
//
//    <S extends T> Iterable<S> saveAll(Iterable<S> var1);
//
//    Optional<T> findById(ID var1);
//
//    boolean existsById(ID var1);
//
//    Iterable<T> findAllById(Iterable<ID> var1);
//
//    long count();
//
//    void deleteById(ID var1);
//
//    void delete(T var1);
//
//    void deleteAllById(Iterable<? extends ID> var1);
//
//    void deleteAll(Iterable<? extends T> var1);
//
//    void deleteAll();

    List<T> findAll();

//    List<T> findAll(Sort var1);
//
//    void flush();
//
//    <S extends T> S saveAndFlush(S var1);
//
//    <S extends T> List<S> saveAllAndFlush(Iterable<S> var1);
//
//    void deleteAllInBatch(Iterable<T> var1);
//
//    void deleteAllByIdInBatch(Iterable<ID> var1);
//
//    void deleteAllInBatch();
//
    T getById(ID var1);
//
//    <S extends T> List<S> findAll(Example<S> var1);
//
//    <S extends T> List<S> findAll(Example<S> var1, Sort var2);

//    QBean<T> getQBean();
}
