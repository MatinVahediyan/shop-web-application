package com.spring.shop.model;

import com.spring.shop.model.enumeration.EntitlementType;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "Entitlement")
@Table(
        name = "entitlements"
)
public class Entitlement {
    @Id
    @SequenceGenerator(
            name = "entitlements_sequence",
            sequenceName = "entitlements_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "entitlements_sequence"
    )
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(
            name = "type",
            nullable = false
    )
    private EntitlementType type;
    @Column(
            name = "start_date",
            columnDefinition = "TIMESTAMP"
    )
    private Date startDate;
    @Column(
            name = "end_date",
            columnDefinition = "TIMESTAMP"
    )
    private Date endDate;
    @Column(
            name = "never_expires"
    )
    private Boolean neverExpires;
    @Column(
            name = "comments",
            columnDefinition = "VARCHAR(1000)"
    )
    private String comments;
    @OneToOne
    @JoinColumn(
            name = "customer_id",
            referencedColumnName = "id",
            nullable = false,
            foreignKey = @ForeignKey(
                    name = "customer_entitlement_fk"
            )
    )
    private Customer customer;

    public Entitlement() {
    }

    public Entitlement(EntitlementType type,
                       Date startDate,
                       Date endDate,
                       Boolean neverExpires,
                       String comments,
                       Customer customer) {
        this.type = type;
        this.startDate = startDate;
        this.endDate = endDate;
        this.neverExpires = neverExpires;
        this.comments = comments;
        this.customer = customer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EntitlementType getType() {
        return type;
    }

    public void setType(EntitlementType type) {
        this.type = type;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Boolean getNeverExpires() {
        return neverExpires;
    }

    public void setNeverExpires(Boolean neverExpires) {
        this.neverExpires = neverExpires;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Entitlement{" +
                "id=" + id +
                ", type=" + type +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", neverExpires=" + neverExpires +
                ", comments='" + comments + '\'' +
                ", customer=" + customer +
                '}';
    }
}
