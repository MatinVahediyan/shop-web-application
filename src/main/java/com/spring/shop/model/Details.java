package com.spring.shop.model;

import com.spring.shop.model.enumeration.DetailType;

import javax.persistence.*;

@Entity(name = "Details")
@Table(
        name = "details"
)
public class Details {

    @Id
    @SequenceGenerator(
            name = "details_sequence",
            sequenceName = "details_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "details_sequence"
    )
    private Long id;
    @Column(
            name = "street"
    )
    private String street;
    @Column(
            name = "city"
    )
    private String city;
    @Column(
            name = "postal_code"
    )
    private String postalCode;
    @Column(
            name = "state"
    )
    private String state;
    @Column(
            name = "country"
    )
    private String country;
    @Enumerated(
            EnumType.STRING
    )
    private DetailType detailType;
    @ManyToOne
    @JoinColumn(
            name = "customer_id",
            nullable = false,
            referencedColumnName = "id",
            foreignKey = @ForeignKey(
                    name = "customer_detail_fk"
            )
    )
    private Customer customer;

    public Details() {

    }

    public Details(String street,
                   String city,
                   String postalCode,
                   String state,
                   String country,
                   DetailType detailType,
                   Customer customer) {
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
        this.state = state;
        this.country = country;
        this.detailType = detailType;
        this.customer = customer;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public String getStreet() {

        return street;
    }

    public void setStreet(String street) {

        this.street = street;
    }

    public String getCity() {

        return city;
    }

    public void setCity(String city) {

        this.city = city;
    }

    public String getPostalCode() {

        return postalCode;
    }

    public void setPostalCode(String postalCode) {

        this.postalCode = postalCode;
    }

    public String getState() {

        return state;
    }

    public void setState(String state) {

        this.state = state;
    }

    public String getCountry() {

        return country;
    }

    public void setCountry(String country) {

        this.country = country;
    }

    public DetailType getDetailType() {

        return detailType;
    }

    public void setDetailType(DetailType detailType) {

        this.detailType = detailType;
    }

    public Customer getCustomer() {

        return customer;
    }

    public void setCustomer(Customer customer) {

        this.customer = customer;
    }

    @Override
    public String toString() {

        return "Details{" +
                "id=" + id +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", state='" + state + '\'' +
                ", country='" + country + '\'' +
                ", detailType=" + detailType +
                ", customer=" + customer +
                '}';
    }
}
