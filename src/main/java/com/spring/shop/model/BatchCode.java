package com.spring.shop.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "BatchCode")
@Table(
        name = "batch_code"
)
public class BatchCode {
    @Id
    @SequenceGenerator(
            name = "batch_code_sequence",
            sequenceName = "batch_code_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "batch_code_sequence"
    )
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(
            name = "customer_id",
            referencedColumnName = "id",
            nullable = false,
            foreignKey = @ForeignKey(
                    name = "customer_batch_fk"
            )
    )
    private Customer customer;
    @OneToMany(
            mappedBy = "batchCode",
            orphanRemoval = true,
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private List<Feature> features = new ArrayList<>();

    @OneToMany(
            mappedBy = "batchCode",
            orphanRemoval = true,
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private List<Product> products = new ArrayList<>();


    public BatchCode() {
    }

    public BatchCode(Customer customer,
                     List<Feature> features,
                     List<Product> products) {
        this.customer = customer;
        this.features = features;
        this.products = products;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "BatchCode{" +
                "id=" + id +
                ", customer=" + customer +
                ", features=" + features +
                ", products=" + products +
                '}';
    }
}
