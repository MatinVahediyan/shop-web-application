package com.spring.shop.model;

import com.spring.shop.model.enumeration.CustomerType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Customer")
@Table(
        name = "customers",
        uniqueConstraints = {
                @UniqueConstraint(
                        name = "email_unique_key",
                        columnNames = "email"
                )
                ,
                @UniqueConstraint(
                        name = "phone_number_unique_key",
                        columnNames = "phone_number"
                )
                ,
                @UniqueConstraint(
                        name = "phone_unique_key",
                        columnNames = "phone"
                )
        }
)
public class Customer {

    @Id
    @SequenceGenerator(
            name = "customer_sequence",
            sequenceName = "customer_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "customer_sequence"
    )
    private Long id;

    @Enumerated(EnumType.STRING)
    private CustomerType type;

    @OneToMany(
            mappedBy = "customer",
            orphanRemoval = true,
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private List<Details> details = new ArrayList<>();

    @Column(
            name = "description",
            nullable = false,
            columnDefinition = "VARCHAR(1000)"
    )
    private String description;

    @OneToOne(
            mappedBy = "customer",
            orphanRemoval = true,
            cascade = CascadeType.ALL
    )
    private BatchCode batchCode;

    @OneToOne(
            mappedBy = "customer",
            orphanRemoval = true,
            cascade = CascadeType.ALL
    )
    private Entitlement entitlement;

    public Customer() {

    }

    public Customer(CustomerType type,
                    List<Details> details,
                    String description,
                    BatchCode batchCode,
                    Entitlement entitlement) {
        this.type = type;
        this.details = details;
        this.description = description;
        this.batchCode = batchCode;
        this.entitlement = entitlement;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CustomerType getType() {
        return type;
    }

    public void setType(CustomerType type) {
        this.type = type;
    }

    public List<Details> getDetails() {
        return details;
    }

    public void setDetails(List<Details> details) {
        this.details = details;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BatchCode getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(BatchCode batchCode) {
        this.batchCode = batchCode;
    }

    public Entitlement getEntitlement() {
        return entitlement;
    }

    public void setEntitlement(Entitlement entitlement) {
        this.entitlement = entitlement;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", type=" + type +
                ", details=" + details +
                ", description='" + description + '\'' +
                ", batchCode=" + batchCode +
                ", entitlement=" + entitlement +
                '}';
    }
}
