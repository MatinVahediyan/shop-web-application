package com.spring.shop.contact;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.querydsl.jpa.impl.JPAQuery;
import com.spring.shop.dto.ContactDTO;
import com.spring.shop.model.Contact;
import com.spring.shop.common.QdslRepository;
import com.spring.shop.model.QContact;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ContactRepository implements QdslRepository<Contact, Long> {
    public static QContact qcontact = QContact.contact;

//    public static final QBean<Contact> Q_BEAN = Projections.bean(Contact.class,
//            qcontact.id,
//            qcontact.firstName,
//            qcontact.lastName,
//            qcontact.middleName,
//            qcontact.email,
//            qcontact.local,
//            qcontact.company.id);

    public static final QBean<ContactDTO> Q_BEAN = Projections.bean(ContactDTO.class,
            qcontact.id,
            qcontact.firstName,
            qcontact.lastName,
            qcontact.middleName,
            qcontact.email,
            qcontact.local,
            qcontact.company.id.as("companyId"));

    @PersistenceContext
    private EntityManager entityManager;

    private JPAQuery<ContactDTO> jpaQuery;

    public ContactRepository() {
        this.jpaQuery = new JPAQuery<>(entityManager);
    }


    @Override
    public List<Contact> findAll() {
        List<ContactDTO> contactDTOList = jpaQuery
                .select(Q_BEAN)
                .from(qcontact)
                .fetch();
        List<Contact> contacts = new ArrayList<>();

        for (ContactDTO instance : contactDTOList) {
            Contact contact = new Contact(
                    instance.getId(),
                    instance.getFirstName(),
                    instance.getLastName(),
                    instance.getMiddleName(),
                    instance.getEmail(),
                    instance.getLocal()
            );
            contact.setCompany(
                    jpaQuery
                            .select(qcontact.company)
                            .from(qcontact)
                            .where(qcontact.company.id.eq(instance.getCompanyId()))
                            .fetch()
                            .get(0)
            );
            contacts.add(contact);
        }
        return contacts;
    }

    @Override
    public Contact getById(Long id) {
        List<ContactDTO> contactDTOList = jpaQuery
                .select(Q_BEAN)
                .from(qcontact)
                .where(qcontact.id.eq(id))
                .fetch();
        ContactDTO dto = contactDTOList.get(0);
        Contact contact = new Contact(
                dto.getId(),
                dto.getFirstName(),
                dto.getLastName(),
                dto.getMiddleName(),
                dto.getEmail(),
                dto.getLocal()
        );
        contact.setCompany(
                jpaQuery
                        .select(qcontact.company)
                        .from(qcontact)
                        .where(qcontact.company.id.eq(dto.getCompanyId()))
                        .fetch()
                        .get(0)
        );
        return contact;
    }

//    @Override
//    public QBean<Contact> getQBean() {
//        return Q_BEAN;
//    }

}
