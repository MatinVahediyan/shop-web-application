package com.spring.shop.model.enumeration;

public enum EntitlementType {
    HardwareKey,
    ProductKey,
    ProtectionKeyUpdate
}
