package com.spring.shop.model.enumeration;

public enum DetailType {

    billing ,
    shipping
}
