package com.spring.shop.model;

import javax.persistence.*;

@Entity(name = "Contact")
@Table(
        name = "contacts",
        uniqueConstraints = {
                @UniqueConstraint(
                        name = "contact_email_unique",
                        columnNames = "email"
                )
        }
)
public class Contact {

    @Id
    @SequenceGenerator(
            name = "sequence_contact",
            sequenceName = "contact_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "contact_sequence"
    )
    private Long id;
    @Column(
            name = "first_name",
            nullable = false
    )
    private String firstName;
    @Column(
            name = "last_name",
            nullable = false
    )
    private String lastName;
    @Column(
            name = "middle_name"
    )
    private String middleName;
    @Column(
            name = "email"
    )
    private String email;
    @Column(
            name = "local",
            columnDefinition = "VARCHAR(255) DEFAULT 'English'"
    )
    private String local;

    @ManyToOne
    @JoinColumn(
            name = "company_id",
            nullable = false,
            referencedColumnName = "id",
            foreignKey = @ForeignKey(
                    name = "company_contact_fk"
            )
    )
    private Company company;

    public Contact() {
    }

    public Contact(String firstName,
                   String lastName,
                   String middleName,
                   String email,
                   String local,
                   Company company) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.email = email;
        this.local = local;
        this.company = company;
    }

    public Contact(Long id,
                   String firstName,
                   String lastName,
                   String middleName,
                   String email,
                   String local) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.email = email;
        this.local = local;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", email='" + email + '\'' +
                ", local='" + local + '\'' +
                ", company=" + company +
                '}';
    }
}

